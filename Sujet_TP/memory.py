import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
    Prend le jeu de carte et le mélange de manière aléatoire.

    Parameters
    ----------
    Tab : array
        Contient toutes les cartes du jeu.

    Returns
    -------
    Tab : array
        Contient toutes les cartes du jeu mélangées.

    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]

    return Tab

def carte_cache(Tab):   #TODO
    """
        TODO
        Doit créer une liste de même longueur que la liste renvoyés dans la question précédentes
    """
    cache=[]
    for i in range (len(Tab)):
        cache.append(i+1)
    return cache
        
        


def choisir_cartes(Tab):
    """
    fonction demandant deux cartes à renvoyer
    
    Parameters
    ----------
    Tab : TYPE tableau
        tableau jeu de carte

    Returns
    -------
    list
        renvoi deux cartes choisies parmi le jeu de carte

    """

    c1 = int(input("Choisissez une carte : "))-1
    print(Tab[c1])  
    c2 = int(input("Choisissez une deuxieme carte : "))-1
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(int("Veuillez choisir une deuxieme carte différente de la premiere : "))-1

    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        TODO
        doit retrouner les cartes dans la liste cachée
    """
    
    Tab_cache[c1]=Tab[c1]
    Tab_cache[c2]=Tab[c2]
    return  Tab_cache
        
        
def test_unitaire_melange(T1):
    #test unitaire mélange carte
    if melange_carte(T1)==T1:
        B=True;
    else :
        B=False;
    return B
    

def jouer(Tab):
    """
    fonction permettant de jouer au Memory

    Parameters
    ----------
    Tab : TYPE tableau
        jeu de carte sous forme de tableau
    Returns
    -------
    None.

    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Mélangé :",test_unitaire_melange(Tab))
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    if Tab[c1]==Tab[c2]:
            del Tab_cache[c1]
            del Tab_cache[c2]
            
    
    while  len(Tab_cache)!=0:   
        
        Tab_cache =  carte_cache(Tab)
        print(Tab_cache)
        
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
      
        if Tab[c1]==Tab[c2]:
            
            del Tab[0]
            del Tab[1]
           
        print (Tab_cache)
    print("Bravo tu as gagné!!!")



if __name__ == "__main__" :
    choisir_cartes(Tabl)